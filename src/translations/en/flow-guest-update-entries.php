<?php
/**
 * Flow guest update entries plugin for Craft CMS 3.x
 *
 * This plugin allows you to save/update guest entries from the front-end of your website.
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Comunications
 */

/**
 * Flow guest update entries en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('flow-guest-update-entries', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow Comunications
 * @package   FlowGuestUpdateEntries
 * @since     1.0.0
 */
return [
    'Flow guest update entries plugin loaded' => 'Flow guest update entries plugin loaded',
];
